//
//  Command.swift
//  patterns
//
//  Created by apple on 22.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation


class Acoount {
    var accountName: String
    var balance: Int
    init(accountName: String, balance: Int) {
        self.accountName = accountName
        self.balance = balance
    }
}

protocol Command {
    func execute()
    var isComplete: Bool {get set}
}

class Deposit: Command {
    
    private var _account: Acoount
    private var _amount: Int
    var isComplete = false
    
    init(account: Acoount, amount: Int) {
        self._account = account
        self._amount = amount
    }
    
    func execute() {
        self._account.balance += self._amount
        isComplete = true
    }
}

class WithDraw: Command {
    
    private var _account: Acoount
    private var _amount: Int
    var isComplete = false
    
    init(account: Acoount, amount: Int) {
        self._account = account
        self._amount = amount
    }
    
    func execute() {
        if self._account.balance >= self._amount {
            self._account.balance -= self._amount
            isComplete = true
        }
    }
}

class TransactionManager {
    
    static let shared = TransactionManager()
    private init(){}
    private var _transactions:[Command] = []
    
    var pendingTransactions:[Command] {
        get {
            return self._transactions.filter{$0.isComplete == false}
        }
    }
    
    func addTransactions(comand: Command) {
        self._transactions.append(comand)
    }
    
    func processingTransactions() {
        self._transactions.filter{$0.isComplete == false}.forEach{$0.execute()}
    }
}

func patternCommand() {
    
    let account = Acoount(accountName: "Vladimir", balance: 1000)
    let transactionManager = TransactionManager.shared
    transactionManager.addTransactions(comand: Deposit(account: account, amount: 100))
    transactionManager.processingTransactions()
}



