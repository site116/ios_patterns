//
//  AbstractFactory.swift
//  patterns
//
//  Created by apple on 02.11.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation

protocol CarProtocol {
    func driv()
}

class LittleSizeCar: CarProtocol {
    func driv() {
        print("drive a little car")
    }
}

class MiddleSizeCar: CarProtocol {
    func driv() {
        print("drive a middle car")
    }
}


protocol BussProtocol {
      func driv()
}

class LittleSizeBus: BussProtocol {
    func driv() {
         print("drive a littel size car")
    }
}

class MiddleSizeBus: BussProtocol {
    func driv() {
        print("drive a middle size car")
    }
}


protocol Factory1 {
    func produceBuss() -> BussProtocol
    func produceCar() -> CarProtocol
}

class LittleSizeFactory: Factory1 {
    func produceBuss() -> BussProtocol {
        return LittleSizeBus()
    }
    
    func produceCar() -> CarProtocol {
        return LittleSizeCar()
    }
}

class MiddleFactory: Factory1 {
    func produceBuss() -> BussProtocol {
        return MiddleSizeBus()
    }
    
    func produceCar() -> CarProtocol {
        return MiddleSizeCar()
    }
}





