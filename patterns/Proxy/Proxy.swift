//
//  Proxy.swift
//  patterns
//
//  Created by apple on 26.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation

class ProxyUser {
    let name = "Petr"
    let password = "123"
}

protocol ServerProtocol {
    func grandAccess(user: ProxyUser)
    func denyAccess(user: ProxyUser)
}

class ServerSide : ServerProtocol {
    func grandAccess(user: ProxyUser) {
        print("access granted to user with id \(user.name)")
    }
    
    func denyAccess(user: ProxyUser) {
         print("access denided to user with id \(user.name)")
    }
}

class ServerProxy : ServerProtocol {
    private var server : ServerSide!
    func grandAccess(user: ProxyUser) {
        guard server != nil else {
            print("access cant't be granted")
            return
        }
        server.grandAccess(user: user)
    }
    
    func denyAccess(user: ProxyUser) {
        server.denyAccess(user: user)
    }
    
    
    func authenticate(user: ProxyUser) {
        guard user.password == "123" else {return}
        print("user is authenticate")
        server = ServerSide()
        
    }
}


