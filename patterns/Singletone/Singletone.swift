//
//  Singletone.swift
//  patterns
//
//  Created by Владимир on 15.09.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation


class Singletone {
    var counter = 0
    static var singletone : Singletone?
    
    private init() {
        
    }
    
    class func getInstanse() -> Singletone {
        
        if (self.singletone === nil) {
            return Singletone()
        } else {
            return self.singletone!
        }
    }
    
    func add() -> Int {
        self.counter = self.counter + 1
        return self.counter
    }
    
}

