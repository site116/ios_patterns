//
//  Safe.swift
//  patterns
//
//  Created by apple on 19.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation
class Safe {
    static let shared = Safe()
    var money = 0
    private init() {}
}
