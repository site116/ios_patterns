//
//  Decorator.swift
//  patterns
//
//  Created by apple on 15.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation

protocol Porsche {
    func getPrice()->Double
    func getDescriptions()->String
}

class Boxter: Porsche {
    func getPrice() -> Double {
        return 120
    }
    
    func getDescriptions() -> String {
        return "Porcshe Boxter"
    }
}

class PorsheDecorator: Porsche {

    private let decoratedPorsche: Porsche
    required init(dp: Porsche) {
        self.decoratedPorsche=dp
    }
    
    func getPrice() -> Double {
        return decoratedPorsche.getPrice()
    }
    
    func getDescriptions() -> String {
        return decoratedPorsche.getDescriptions()
    }
}


class PremiumAudioSystem: PorsheDecorator {
    
    required init(dp: Porsche) {
        super.init(dp: dp)
    }
    
    override func getPrice() -> Double {
        return super.getPrice()+30
    }
    
    override func getDescriptions() -> String {
        return super.getDescriptions() + "with premium audio system"
    }
}


class PanoramicSunroof: PorsheDecorator {
    required init(dp: Porsche) {
        super.init(dp: dp)
    }
    
    override func getPrice() -> Double {
        return super.getPrice()+20
    }
    
    override func getDescriptions() -> String {
        return super.getDescriptions() + "with panoramic sunroof"
    }
}
