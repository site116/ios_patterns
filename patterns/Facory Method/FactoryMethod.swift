//
//  FactoryMethod.swift
//  patterns
//
//  Created by apple on 16.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation

protocol Vehicle {
    func drive()
}

class Car: Vehicle {
    func drive() {
        print("drive a car")
    }
}

class Truck: Vehicle {
    func drive() {
        print("drive a truck")
    }
}

class Bass: Vehicle {
    func drive() {
        print("drive a bass")
    }
}


protocol VehicalFactory {
    func produce()->Vehicle
}


class CarFactory: VehicalFactory {
    func produce() -> Vehicle {
        print("car is created")
        return Car()
    }
}

class TruckFactory: VehicalFactory {
    func produce() -> Vehicle {
        print("truck is created")
        return Truck()
    }
}

class BassFactory: VehicalFactory {
    func produce() -> Vehicle {
        print("bas is created")
        return Bass()
    }
}

