//
//  ViewController.swift
//  patterns
//
//  Created by Владимир on 15.09.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//



import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // стратегия
        let human = Human(sb: NonSwimmer(), db: ProffesionalDiver())
        human.performDive()
        human.performSwim()
        
        // Наблюдатель
        let teacher = Teacher()
        let pupil = Pupil()
        teacher.add(observer: pupil)
        teacher.homeTask="New homework #3"
        
        // Декоратор
        var porsheBoxter: Porsche = Boxter()
        print(porsheBoxter.getDescriptions(),porsheBoxter.getPrice())
        
        porsheBoxter = PremiumAudioSystem(dp: porsheBoxter)
        print(porsheBoxter.getDescriptions(),porsheBoxter.getPrice())
        
        porsheBoxter = PanoramicSunroof(dp: porsheBoxter)
        print(porsheBoxter.getDescriptions(),porsheBoxter.getPrice())
        
        // Простая фабрика
        let storeNY = NYPizzaStore()
        storeNY.orderPizza(type: "cheese")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

