import Foundation
import UIKit

// Определяет семейство алгоритмов, инкапсулирует каждый
// из них и обеспечивает их взаимозаменяемость. Он позволяет модфицировать алгоритмы независимо
// от их использования на стороен клиента

// протокол
protocol SwimBeheviar {
    func swim()
}

class ProffesionalSwimmer: SwimBeheviar {
    func swim() {
        print("professional swimming")
    }
}

class NonSwimmer: SwimBeheviar {
    func swim(){
        print("non-swimming")
    }
}

// протокол
protocol DiveBeheviar {
    func dive()
}

class ProffesionalDiver: DiveBeheviar {
    func dive() {
        print("proffesional diving")
    }
}

class NewbiewDiver: DiveBeheviar {
    func dive() {
        print("newbie diving")
    }
}

class NonDiver: DiveBeheviar {
    func dive() {
        print("no-diving")
    }
}

// класс
class Human {
    
    private var diveBehavior: DiveBeheviar!
    private var swimBehavior: SwimBeheviar!
    
    init(sb: SwimBeheviar, db: DiveBeheviar) {
        self.diveBehavior = db;
        self.swimBehavior = sb;
    }
    
    // делегирование метода
    func performSwim(){
        swimBehavior.swim()
    }
    // делегирование метода
    func performDive(){
        diveBehavior.dive()
    }
    
    // позволяет динамически устанавливать поведение
    func setSwimBehavior(sb: SwimBeheviar){
        self.swimBehavior = sb;
    }
    
    func setDiveBehavior(db: DiveBeheviar){
        self.diveBehavior = db
    }
    
    func run(){
        print("ranning")
    }
}









