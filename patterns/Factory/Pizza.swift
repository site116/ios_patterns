//
//  Pizza.swift
//  patterns
//
//  Created by Владимир on 17.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation


// пиццы
class CheesePizza: Pizza {
    func prepare() {
        print("prepare cheese pizza")
    }
}

class PeperonyPizza: Pizza {
    func prepare() {
        print("prepare peperony pizza")
    }
}

class ClamPizza: Pizza {
    func prepare() {
        print("prepare clam pizza")
    }
}

class VeggiePizza: Pizza {
    func prepare() {
        print("prepare veggie pizza")
    }
}

