//
//  Factory.swift
//  patterns
//
//  Created by Владимир on 16.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation


// протокол для пиццы
protocol Pizza {
    func prepare()
}

// протокол фабрики
protocol PizzaStore {
    // фаьричный метод создает новый класс
    func createPizza(type: String) -> Pizza?
    // другие методы
    func orderPizza(type: String)
}

extension PizzaStore {
    func orderPizza(type: String) {
        
        if let pizza = self.createPizza(type: type) {
             pizza.prepare()
        } else {
            print("pizza is nil")
        }
    }
}


class NYPizzaStore: PizzaStore {

    func createPizza(type: String) -> Pizza? {
        var pizza: Pizza?
        switch type {
            case "cheese": pizza = CheesePizza()
            case "peperony": pizza = PeperonyPizza()
            default:
              pizza = nil
        }
        return pizza
    }
}


class ChicagoStore: PizzaStore {
    
    func createPizza(type: String) -> Pizza? {
        var pizza: Pizza?
        switch type {
            case "clam": pizza = ClamPizza()
            case "veggie": pizza = VeggiePizza()
            default:
                pizza = nil
        }
        return pizza
    }
}




