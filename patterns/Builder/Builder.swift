//
//  Builder.swift
//  patterns
//
//  Created by apple on 30.10.2018.
//  Copyright © 2018 Владимир. All rights reserved.
//

import Foundation
import UIKit

protocol ThemePrtocol {
    var backgroundcolor : UIColor { get }
    var textColor : UIColor { get }
}

class Theme: ThemePrtocol {
    
    var backgroundcolor: UIColor
    var textColor : UIColor
    
    init(backgroundcolor: UIColor, textColor: UIColor) {
        self.backgroundcolor = backgroundcolor
        self.textColor = textColor
    }
}

protocol ThemeBuilderProtocol {
    func setBackgroun(color: UIColor)
    func setText(color: UIColor)
    func createTheme() -> ThemePrtocol?
}


class ThemeBuilder: ThemeBuilderProtocol {
   
    private var backgroundcolor : UIColor?
    private var textColor : UIColor?
    
    func setBackgroun(color: UIColor) {
        self.backgroundcolor = color
    }
    
    func setText(color: UIColor) {
        self.textColor = color
    }
    
    func createTheme() -> ThemePrtocol? {
        guard let bgColor = self.backgroundcolor, let txColor = self.textColor else {return nil}
        return Theme(backgroundcolor: bgColor, textColor: txColor)
    }
}






